package com.martindekock.project;

public class PersonOfInterest {


    private String id, occupation, templarID, firstName, lastName;

    public PersonOfInterest(String firstName, String lastName, String occupation, String templarID) {
        this.id = firstName + lastName;
        this.occupation = occupation;
        this.templarID = templarID;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getTemplarID() {
        return templarID;
    }

    public void setTemplarID(String templarID) {
        this.templarID = templarID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return firstName + "  " + lastName + "  " + occupation;
    }
}
