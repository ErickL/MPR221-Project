package com.martindekock.project;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.MyViewHolder> {

    private ArrayList<Contact> contactList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, surname, contactnumber;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.name = itemView.findViewById(R.id.name);
            this.surname = itemView.findViewById(R.id.surname);
            this.contactnumber = itemView.findViewById(R.id.contactnumber);
        }


    }

    public ContactsAdapter(ArrayList<Contact> contactList) {
        this.contactList = contactList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_list_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Contact contact = contactList.get(position);
        holder.name.setText(contact.getFirstName());
        holder.surname.setText(contact.getLastName());
        holder.contactnumber.setText(contact.getContactNumber());
    }

    @Override
    public int getItemCount() {
        return contactList.size();
    }


}
