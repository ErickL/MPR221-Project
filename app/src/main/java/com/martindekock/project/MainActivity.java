package com.martindekock.project;

import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    // - - - - - - - - - - - Views - - - - - - - - - - -
    private DrawerLayout mDrawerLayout;
    private ConstraintLayout contactLayout, operationsLayout, locationsLayout, targetLayout, personLayout;
    private ListView contactsListView, operationsListView, locationsListView, targetListView, personListView;


    // - - - - - - - - - - - Lists - - - - - - - - - - -
    private ArrayList<Contact> contactList;
    private ArrayList<Operations> operationsList;
    private ArrayList<PersonOfInterest> personList;
    private ArrayList<Locations> locationsList;
    private ArrayList<Target> targetList;


    // - - - - - - - - - - - Adapters - - - - - - - - - - -
    private ArrayAdapter<Contact> contactAdapter;
    private ArrayAdapter<Operations> operationAdapter;
    private ArrayAdapter<PersonOfInterest> personAdapter;
    private ArrayAdapter<Locations> locationAdapter;
    private ArrayAdapter<Target> targetAdapter;


    // - - - - - - - - - - - Database - - - - - - - - - - -
    private TemplarDB db;


    private RecyclerView contactRecyclerView, operationsRecyclerView;
    private ContactsAdapter contactsAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        // ==============================  Setting Values  ==============================

        contactLayout = findViewById(R.id.contactsLayout);
        operationsLayout = findViewById(R.id.operationsLayout);
        locationsLayout = findViewById(R.id.locationsLayout);
        targetLayout = findViewById(R.id.targetLayout);
        personLayout = findViewById(R.id.personLayout);

        contactsListView = findViewById(R.id.contactListView);
        operationsListView = findViewById(R.id.operationsListView);
        locationsListView = findViewById(R.id.locationsListView);
        targetListView = findViewById(R.id.targetListView);
        personListView = findViewById(R.id.personListView);


        db = new TemplarDB(this);


        // ==============================  Setting Lists  ==============================

        contactList = db.SelectContactList();
        operationsList = db.SelectOperationList();
        personList = db.SelectPersonOfInterestList();
        locationsList = db.SelectLocationList();
        targetList = db.SelectTargetList();

        // ==============================  Setting Adapters  ==============================

        contactAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, contactList);
        operationAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, operationsList);
        personAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, personList);
        locationAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, locationsList);
        targetAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, targetList);

        // ==============================  Adding Adapters to list view =====================

        contactsListView.setAdapter(contactAdapter);
        operationsListView.setAdapter(operationAdapter);
        personListView.setAdapter(personAdapter);
        locationsListView.setAdapter(locationAdapter);
        targetListView.setAdapter(targetAdapter);


        // ============================== RecyclerView =====================


        contactRecyclerView = findViewById(R.id.contactRecyclerView);
        contactsAdapter = new ContactsAdapter(contactList);


        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        contactRecyclerView.setLayoutManager(mLayoutManager);
        contactRecyclerView.setItemAnimator(new DefaultItemAnimator());
        contactRecyclerView.setAdapter(contactsAdapter);
        contactsAdapter.notifyDataSetChanged();


        contactRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), contactRecyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Contact contact = contactList.get(position);
                Toast.makeText(getApplicationContext(), contact.getFirstName() + " is selected!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        // ==============================  Navigation Drawer  ==============================

        Toolbar toolbar = findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);

        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().

                setTitle("");
        actionbar.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);

        mDrawerLayout =

                findViewById(R.id.drawer_layout);

        mDrawerLayout.addDrawerListener(
                new DrawerLayout.DrawerListener()

                {
                    @Override
                    public void onDrawerSlide(View drawerView, float slideOffset) {
                        // Respond when the drawer's position changes
                    }

                    @Override
                    public void onDrawerOpened(View drawerView) {
                        // Respond when the drawer is opened
                    }

                    @Override
                    public void onDrawerClosed(View drawerView) {
                        // Respond when the drawer is closed
                    }

                    @Override
                    public void onDrawerStateChanged(int newState) {
                        // Respond when the drawer motion state changes
                    }
                }
        );

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener()

                {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        // set item as selected to persist highlight
                        menuItem.setChecked(true);
                        // close drawer when item is tapped
                        mDrawerLayout.closeDrawers();


                        switch (menuItem.getTitle().toString()) {
                            case "Contacts":
                                operationsLayout.setVisibility(View.INVISIBLE);
                                locationsLayout.setVisibility(View.INVISIBLE);
                                personLayout.setVisibility(View.INVISIBLE);
                                targetLayout.setVisibility(View.INVISIBLE);
                                contactLayout.setVisibility(View.VISIBLE);
                                break;

                            case "Operations":
                                contactLayout.setVisibility(View.INVISIBLE);
                                locationsLayout.setVisibility(View.INVISIBLE);
                                personLayout.setVisibility(View.INVISIBLE);
                                targetLayout.setVisibility(View.INVISIBLE);
                                operationsLayout.setVisibility(View.VISIBLE);
                                break;

                            case "Locations":
                                operationsLayout.setVisibility(View.INVISIBLE);
                                contactLayout.setVisibility(View.INVISIBLE);
                                personLayout.setVisibility(View.INVISIBLE);
                                targetLayout.setVisibility(View.INVISIBLE);
                                locationsLayout.setVisibility(View.VISIBLE);
                                break;

                            case "Persons of Interest":
                                locationsLayout.setVisibility(View.INVISIBLE);
                                operationsLayout.setVisibility(View.INVISIBLE);
                                contactLayout.setVisibility(View.INVISIBLE);
                                targetLayout.setVisibility(View.INVISIBLE);
                                personLayout.setVisibility(View.VISIBLE);
                                break;

                            case "Targets":
                                personLayout.setVisibility(View.INVISIBLE);
                                locationsLayout.setVisibility(View.INVISIBLE);
                                operationsLayout.setVisibility(View.INVISIBLE);
                                contactLayout.setVisibility(View.INVISIBLE);
                                targetLayout.setVisibility(View.VISIBLE);
                                break;

                            default:
                                personLayout.setVisibility(View.INVISIBLE);
                                locationsLayout.setVisibility(View.INVISIBLE);
                                operationsLayout.setVisibility(View.INVISIBLE);
                                contactLayout.setVisibility(View.INVISIBLE);
                                targetLayout.setVisibility(View.INVISIBLE);
                        }

                        // Add code here to update the UI based on the item selected
                        // For example, swap UI fragments here

                        return true;
                    }
                });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
