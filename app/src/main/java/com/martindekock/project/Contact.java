package com.martindekock.project;

public class Contact {

    private String id, contactNumber, firstName, lastName, templarID;

    public Contact(String firstName, String lastName, String contactNumber, String templarID) {
        this.id = firstName + lastName;
        this.contactNumber = contactNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.templarID = templarID;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTemplarID() {
        return templarID;
    }

    public void setTemplarID(String templarID) {
        this.templarID = templarID;
    }

    @Override
    public String toString() {
        return firstName + "  " + lastName;


    }
}
