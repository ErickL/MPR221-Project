package com.martindekock.project;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

public class TemplarDB {
    private DatabaseHelper dbHelper;
    private SQLiteDatabase db;

    public TemplarDB(Context context) {
        dbHelper = new DatabaseHelper(context);
        db = dbHelper.getWritableDatabase();
    }
    //=============================================================== CLEAR TABLES ===============================================================//
    public void Delete(){
        db.execSQL("DELETE FROM templar");
        db.execSQL("DELETE FROM personofinterest");
        db.execSQL("DELETE FROM contact");
        db.execSQL("DELETE FROM operation");
        db.execSQL("DELETE FROM location");
        db.execSQL("DELETE FROM target");
    }

    //=============================================================== INSERTS ===============================================================//

    public void AddTemplar(Templar templar) {
        ContentValues values = new ContentValues();
        values.put("username", templar.getUserName());
        values.put("password", templar.getPassword());
        db.insert("templar", null, values);
        Log.i("Added templar", values.toString());
    }

    public void AddPersonOfInterest(PersonOfInterest poi) {
        ContentValues values = new ContentValues();
        values.put("personID", poi.getId());
        values.put("occupation", poi.getOccupation());
        values.put("firstName", poi.getFirstName());
        values.put("lastName", poi.getLastName());
        values.put("templarID", poi.getTemplarID());
        db.insert("personofinterest", null, values);
        Log.i("Added Person", values.toString());
    }

    public void AddContact(Contact contact) {
        ContentValues values = new ContentValues();
        values.put("contactID", contact.getId());
        values.put("firstName", contact.getFirstName());
        values.put("lastName", contact.getLastName());
        values.put("contactNumber", contact.getContactNumber());
        values.put("templarID", contact.getTemplarID());
        db.insert("contact", null, values);
        Log.i("Added Contact", values.toString());
    }

    public void AddOperation(Operations operation) {
        ContentValues values = new ContentValues();
        values.put("operationID", operation.getOperationID());
        values.put("templarID", operation.getTemplarID());
        values.put("targetID", operation.getTargetID());
        values.put("locationID", operation.getLocationID());
        db.insert("operation", null, values);
        Log.i("Added operation", values.toString());
    }

    public void AddLocation(Locations location) {
        ContentValues values = new ContentValues();
        values.put("locationID", location.getName());
        values.put("longitude", location.getLongitude());
        values.put("latitude", location.getLatitude());
        db.insert("location", null, values);
        Log.i("Added location", values.toString());
    }

    public void AddTarget(Target target) {
        ContentValues values = new ContentValues();
        values.put("targetID", target.getName());
        db.insert("target", null, values);
        Log.i("Added target", values.toString());
    }


    //=============================================================== SELECTS ===============================================================//

    public Cursor SelectTemplars() {
        String[] cols = new String[]{"username", "password"};
        Cursor mCursor = db.query(true, "templar", cols, null
                , null, null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    public ArrayList<Templar> SelectTemplarList() {
        ArrayList<Templar> list = new ArrayList<>();
        Cursor c = db.rawQuery("SELECT * FROM templar", null);

        if (c.moveToFirst()) {
            do {
                list.add(new Templar(c.getString(0), c.getString(1)));
                Log.i("Added to list", c.getString(0) + " " + c.getString(1));
            }
            while (c.moveToNext());
        }
        c.close();
        return list;

    }


    public Cursor SelectPersonOfInterest() {
        String[] cols = new String[]{"personID", "occupation", "firstName", "lastName", "templarID"};
        Cursor mCursor = db.query(true, "personofinterest", cols, null
                , null, null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    public ArrayList<PersonOfInterest> SelectPersonOfInterestList() {
        ArrayList<PersonOfInterest> list = new ArrayList<>();
        Cursor c = db.rawQuery("SELECT * FROM personofinterest", null);
        if (c.moveToFirst()) {
            do {
                list.add(new PersonOfInterest(c.getString(0), c.getString(1), c.getString(2), c.getString(3)));
                Log.i("Added to list", c.getString(0) + " " + c.getString(1));
            }
            while (c.moveToNext());
        }
        c.close();
        return list;
    }


    public Cursor SelectContact() {
        String[] cols = new String[]{"contactID", "firstName", "lastName", "contactNumber", "templarID"};
        Cursor mCursor = db.query(true, "contact", cols, null
                , null, null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }


    public ArrayList<Contact> SelectContactList() {
        ArrayList<Contact> list = new ArrayList<>();
        Cursor c = db.rawQuery("SELECT * FROM contact", null);
        if (c.moveToFirst()) {
            do {
                list.add(new Contact(c.getString(1), c.getString(2), c.getString(3), c.getString(4)));
                Log.i("Added to list", c.getString(0) + " " + c.getString(1));
            }
            while (c.moveToNext());
        }
        c.close();
        return list;
    }


    public Cursor SelectOperation() {
        String[] cols = new String[]{"operationID", "templarID", "targetID", "locationID"};
        Cursor mCursor = db.query(true, "operation", cols, null
                , null, null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    public ArrayList<Operations> SelectOperationList() {
        ArrayList<Operations> list = new ArrayList<>();
        Cursor c = db.rawQuery("SELECT * FROM operation", null);
        if (c.moveToFirst()) {
            do {
                list.add(new Operations(c.getString(0), c.getString(1), c.getString(2), c.getString(3)));
                Log.i("Added to list", c.getString(0) + " " + c.getString(1));
            }
            while (c.moveToNext());
        }
        c.close();
        return list;
    }

    public Cursor SelectLocation() {
        String[] cols = new String[]{"locationID", "longitude", "latitude"};
        Cursor mCursor = db.query(true, "location", cols, null
                , null, null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor; // iterate to get each value.
    }

    public ArrayList<Locations> SelectLocationList() {
        ArrayList<Locations> list = new ArrayList<>();
        Cursor c = db.rawQuery("SELECT * FROM location", null);
        if (c.moveToFirst()) {
            do {
                list.add(new Locations(c.getString(0), c.getString(1), c.getString(2)));
                Log.i("Added to list", c.getString(0) + " " + c.getString(1));
            }
            while (c.moveToNext());
        }
        c.close();
        return list;
    }

    public Cursor SelectTarget() {
        String[] cols = new String[]{"targetID"};
        Cursor mCursor = db.query(true, "target", cols, null
                , null, null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    public ArrayList<Target> SelectTargetList() {
        ArrayList<Target> list = new ArrayList<>();
        Cursor c = db.rawQuery("SELECT * FROM target", null);
        if (c.moveToFirst()) {
            do {
                list.add(new Target(c.getString(0)));
                Log.i("Added to list", c.getString(0));
            }
            while (c.moveToNext());
        }
        c.close();
        return list;
    }


    //=============================================================== UPDATES ===============================================================//


    public void UpdateTemplar(Templar templar) {
        ContentValues values = new ContentValues();
        values.put("username", templar.getUserName());
        values.put("password", templar.getPassword());
        db.update("templar", values, "username = " + templar.getUserName(), null);
        Log.i("Templar Updated: ", values.toString());
    }

    public void UpdatePersonOfInterest(PersonOfInterest poi) {
        ContentValues values = new ContentValues();
        values.put("personID", poi.getId());
        values.put("occupation", poi.getOccupation());
        values.put("firstName", poi.getFirstName());
        values.put("lastName", poi.getLastName());
        values.put("templarID", poi.getTemplarID());
        db.update("personofinterest", values, "personID = " + poi.getId(), null);
        Log.i("Person Updated: ", values.toString());
    }

    public void UpdateContact(Contact contact) {
        ContentValues values = new ContentValues();
        values.put("contactID", contact.getId());
        values.put("firstName", contact.getFirstName());
        values.put("lastName", contact.getLastName());
        values.put("contactNumber", contact.getContactNumber());
        values.put("templarID", contact.getTemplarID());
        db.update("contact", values, "contactID = " + contact.getId(), null);
        Log.i("Contact Updated: ", values.toString());
    }

    public void UpdateOperation(Operations operation) {
        ContentValues values = new ContentValues();
        values.put("operationID", operation.getOperationID());
        values.put("templarID", operation.getTemplarID());
        values.put("targetID", operation.getTargetID());
        values.put("locationID", operation.getLocationID());
        db.update("operation", values, "operationID = " + operation.getOperationID(), null);
        Log.i("Operation Updated: ", values.toString());
    }

    public void UpdateLocation(Locations location) {
        ContentValues values = new ContentValues();
        values.put("locationID", location.getName());
        values.put("longitude", location.getLongitude());
        values.put("latitude", location.getLatitude());
        db.update("location", values, "locationID = " + location.getName(), null);
        Log.i("Location Updated: ", values.toString());
    }

    public void UpdateTarget(Target target) {
        ContentValues values = new ContentValues();
        values.put("targetID", target.getName());
        db.update("target", values, "targetID = " + target.getName(), null);
        Log.i("Target Updated: ", values.toString());
    }


    //=============================================================== DELETES ===============================================================//


    public void DeleteTemplar(Templar templar) {
        db.delete("templar", "username = " + templar.getUserName(), null);
    }

    public void DeletePersonOfInterest(PersonOfInterest poi) {
        db.delete("personofinterest", "personID = " + poi.getId(), null);
    }

    public void DeleteContact(Contact contact) {
        db.delete("contact", "contactID = " + contact.getId(), null);
    }

    public void DeleteOperation(Operations operation) {
        db.delete("operation", "operationID = " + operation.getOperationID(), null);
    }

    public void DeleteLocation(Locations location) {
        db.delete("location", "locationID = " + location.getName(), null);
    }

    public void DeleteTarget(Target target) {
        db.delete("target", "targetID = " + target.getName(), null);
    }
}
