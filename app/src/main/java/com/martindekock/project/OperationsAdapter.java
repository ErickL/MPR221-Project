package com.martindekock.project;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class OperationsAdapter extends RecyclerView.Adapter<OperationsAdapter.MyViewHolder> {

    private ArrayList<Operations> operationList;

    public OperationsAdapter(ArrayList<Operations> operationList) {
        this.operationList = operationList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.operation_list_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Operations operation = operationList.get(position);
        holder.name.setText(operation.getOperationID());
        holder.surname.setText(operation.getTargetID());
        holder.contactnumber.setText(operation.getTargetID());
    }

    @Override
    public int getItemCount() {
        return operationList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, surname, contactnumber;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.name = itemView.findViewById(R.id.name);
            this.surname = itemView.findViewById(R.id.surname);
            this.contactnumber = itemView.findViewById(R.id.contactnumber);
        }


    }


}
