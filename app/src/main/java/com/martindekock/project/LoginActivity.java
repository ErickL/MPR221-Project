package com.martindekock.project;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

public class LoginActivity extends AppCompatActivity {
    EditText edtUserName, edtPassword;

    public void ClickLogin(View view) {
        edtUserName = findViewById(R.id.edtUserName);
        edtPassword = findViewById(R.id.edtPassword);
        String resetUser = "";
        String toast = "You are not a Templar!";
        TemplarDB db = new TemplarDB(this);


        ArrayList<Templar> list = db.SelectTemplarList();


        Intent intent = new Intent(getApplicationContext(), MainActivity.class);

        for (Templar item : list) {
            if (item.getUserName().equals(edtUserName.getText().toString())) {
                if (item.getPassword().equals(edtPassword.getText().toString())) {
                    toast = "Welcome";
                    startActivity(intent);
                    finish();
                } else {
                    edtPassword.setText("");
                    Toast.makeText(this, "Incorrect Password", Toast.LENGTH_LONG).show();


                }
                resetUser = edtUserName.getText().toString();
            }


        }
        if (resetUser == "") {
            edtUserName.requestFocus();
        } else {
            edtPassword.requestFocus();
        }

        edtUserName.setText(resetUser);
        edtPassword.setText("");


        Toast.makeText(this, toast, Toast.LENGTH_LONG).show();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        // TemplarDB db = new TemplarDB(this);

        // Templar templar = new Templar("Martin", "123");
        /*Templar templar1 = new Templar("Erick", "123");
        Templar templar2 = new Templar("Johan", "123");*/
        //db.AddTemplar(templar);
       /* db.AddTemplar(templar1);
        db.AddTemplar(templar2);*/



      /*  Contact contact1 = new Contact("0123456789", "Martin", "De Kock", "Martin");
        Contact contact2 = new Contact("6549873210", "Erick", "Lessing", "Erick");
        Contact contact3 = new Contact("7896541235", "Johan", "Van Wyk", "Martin");
        Contact contact4 = new Contact("1235546598", "Firstname", "Lastname", "Johan");
        Contact contact5 = new Contact("0123456789", "Firstname2", "Lastname2", "Martin");
        Contact contact6 = new Contact("0123456789", "Firstname3", "Lastname3", "Johan");
        Contact contact7 = new Contact("0123456789", "Firstname4", "Lastname4", "Erick");
        db.AddContact(contact1);
        db.AddContact(contact2);
        db.AddContact(contact3);
        db.AddContact(contact4);
        db.AddContact(contact5);
        db.AddContact(contact6);
        db.AddContact(contact7);*/

       //TemplarDB db = new TemplarDB(this);
     /*   Operations operation1 = new Operations("Operation1", "MartinDe Kock","Target1", "Location1");
        Operations operation2 = new Operations("Operation2", "ErickLessing","Target2", "Location2");
        Operations operation3 = new Operations("Operation3", "JohanVan Wyk","Target3", "Location3");
        Operations operation4 = new Operations("Operation4", "ErickLessing","Target1", "Location1");
        Operations operation5 = new Operations("Operation5", "MartinDe Kock","Target2", "Location2");
        Operations operation6 = new Operations("Operation6", "ErickLessing","Target3", "Location3");
        Operations operation7 = new Operations("Operation7", "JohanVan Wyk","Target1", "Location1");
        db.AddOperation(operation1);
        db.AddOperation(operation2);
        db.AddOperation(operation3);
        db.AddOperation(operation4);
        db.AddOperation(operation5);
        db.AddOperation(operation6);
        db.AddOperation(operation7);*/

       /* TemplarDB db = new TemplarDB(this);
        PersonOfInterest person1 = new PersonOfInterest("name1", "surname1", "occupation1", "MartinDe Kock");
        PersonOfInterest person2 = new PersonOfInterest("name2", "surname2", "occupation2", "ErickLessing");
        PersonOfInterest person3 = new PersonOfInterest("name3", "surname3", "occupation3", "JohanVan Wyk");
        PersonOfInterest person4 = new PersonOfInterest("name4", "surname4", "occupation4", "MartinDe Kock");
        PersonOfInterest person5 = new PersonOfInterest("name5", "surname5", "occupation5", "ErickLessing");
        PersonOfInterest person6 = new PersonOfInterest("name6", "surname6", "occupation6", "JohanVan Wyk");
        PersonOfInterest person7 = new PersonOfInterest("name7", "surname7", "occupation7", "MartinDe Kock");
        db.AddPersonOfInterest(person1);
        db.AddPersonOfInterest(person2);
        db.AddPersonOfInterest(person3);
        db.AddPersonOfInterest(person4);
        db.AddPersonOfInterest(person5);
        db.AddPersonOfInterest(person6);
        db.AddPersonOfInterest(person7);*/

  //      TemplarDB db = new TemplarDB(this);
       /* Locations loc1 = new Locations("Location1", "long1", "lat1");
        Locations loc2 = new Locations("Location2", "long2", "lat2");
        Locations loc3 = new Locations("Location3", "long3", "lat3");
        Locations loc4 = new Locations("Location4", "long4", "lat4");
        Locations loc5 = new Locations("Location5", "long5", "lat5");
        Locations loc6 = new Locations("Location6", "long6", "lat6");
        Locations loc7 = new Locations("Location7", "long7", "lat7");
        db.AddLocation(loc1);
        db.AddLocation(loc2);
        db.AddLocation(loc3);
        db.AddLocation(loc4);
        db.AddLocation(loc5);
        db.AddLocation(loc6);
        db.AddLocation(loc7);*/

//        TemplarDB db = new TemplarDB(this);
/*      Target target1 = new Target("Target1");
        Target target2 = new Target("Target2");
        Target target3 = new Target("Target3");
        Target target4 = new Target("Target4");
        Target target5 = new Target("Target5");
        Target target6 = new Target("Target6");
        Target target7 = new Target("Target7");
        db.AddTarget(target1);
        db.AddTarget(target2);
        db.AddTarget(target3);
        db.AddTarget(target4);
        db.AddTarget(target5);
        db.AddTarget(target6);
        db.AddTarget(target7);*/

    }
}
