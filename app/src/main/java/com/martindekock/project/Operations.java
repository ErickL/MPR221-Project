package com.martindekock.project;

public class Operations {

    private String operationID, templarID, targetID, locationID;

    public Operations(String operationID, String templarID, String targetID, String locationID) {
        this.operationID = operationID;
        this.templarID = templarID;
        this.targetID = targetID;
        this.locationID = locationID;
    }

    public String getOperationID() {
        return operationID;
    }

    public void setOperationID(String operationID) {
        this.operationID = operationID;
    }

    public String getTemplarID() {
        return templarID;
    }

    public void setTemplarID(String templarID) {
        this.templarID = templarID;
    }

    public String getTargetID() {
        return targetID;
    }

    public void setTargetID(String targetID) {
        this.targetID = targetID;
    }

    public String getLocationID() {
        return locationID;
    }

    public void setLocationID(String locationID) {
        this.locationID = locationID;
    }

    @Override
    public String toString() {
        return operationID + "  " + templarID + "  " + targetID + "  " + locationID;
    }
}
