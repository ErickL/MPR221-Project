package com.martindekock.project;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {


    public DatabaseHelper(Context context) {
        super(context, "TemplarsDB.db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE templar (username TEXT PRIMARY KEY NOT NULL, password TEXT NOT NULL)");
        sqLiteDatabase.execSQL("CREATE TABLE personofinterest (personID TEXT PRIMARY KEY NOT NULL, occupation TEXT, firstName TEXT NOT NULL, lastName TEXT NOT NULL, templarID TEXT NOT NULL, FOREIGN KEY(templarID) REFERENCES templar(username))");
        sqLiteDatabase.execSQL("CREATE TABLE contact (contactID TEXT PRIMARY KEY NOT NULL, firstName TEXT NOT NULL, lastName TEXT NOT NULL, contactNumber TEXT, templarID TEXT, FOREIGN KEY(templarID) REFERENCES templar(username))");
        sqLiteDatabase.execSQL("CREATE TABLE operation (operationID TEXT PRIMARY KEY NOT NULL, templarID TEXT NOT NULL, targetID TEXT NOT NULL, locationID TEXT NOT NULL, FOREIGN KEY(templarID) REFERENCES templar(username), FOREIGN KEY (targetID) REFERENCES target(targetID), FOREIGN KEY(locationID) REFERENCES location(locationID))");
        sqLiteDatabase.execSQL("CREATE TABLE location (locationID TEXT PRIMARY KEY NOT NULL, longitude TEXT, latitude TEXT)");
        sqLiteDatabase.execSQL("CREATE TABLE target (targetID TEXT PRIMARY KEY NOT NULL)");


    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS templar");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS personofinterest");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS contact");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS operation");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS location");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS target");
        onCreate(sqLiteDatabase);
    }
}
